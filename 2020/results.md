# Code Jam 2020

## Qualification Round

* [Round](https://codingcompetitions.withgoogle.com/codejam/round/000000000019fd27)
* [Submissions](https://codingcompetitions.withgoogle.com/codejam/submissions/000000000019fd27/a3V6bWlpZ28)

| Problem                      | Test 1 |  Test 2 |  Test 3 |
|------------------------------|-------:|--------:|--------:|
| Vestigium                    |     7✔ |         |         |
| Nesting Depth                |     5✔ |     11✔ |         |
| Parenting Partnering Returns |     7✔ |     12✔ |         |
| ESAb ATAd                    |     1✔ |      9✔ | ~~16~~✘ |
| Indicium                     |     7✔ | ~~25~~✘ |         |
