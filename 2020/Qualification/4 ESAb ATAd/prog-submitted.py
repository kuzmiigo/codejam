import sys


def complement(a):
    return [(x + 1) % 2 for x in a]


def reverse(a):
    return a[::-1]


def complement_reverse(a):
    return complement(reverse(a))


MAPPING_FUNS = [lambda x: x, complement, reverse, complement_reverse]


def precompute_table():
    table = {}
    for i in range(16):
        s = '{:04b}'.format(i)
        bits = [1 if digit == '1' else 0 for digit in s]
        m = {''.join(map(str, f(bits)[:2])): f for f in MAPPING_FUNS}
        if len(m.keys()) == len(MAPPING_FUNS):
            table[s] = m
    return table


MAPPING = precompute_table()


def detect(bits, i, dst):
    for f in MAPPING_FUNS:
        if f(bits[i:len(bits)-i])[:len(dst)] == dst:
            return f
    return None


class Oracle:
    def __init__(self):
        self.num = 0

    def is_it(self):
        return (self.num + 1) % 10 == 1

    def query(self, i):
        self.num += 1
        print(i, flush=True)
        response = sys.stdin.readline().strip()
        # print('query =', self.num, 'get i =', i, 'response =', response, file=sys.stderr, flush=True)
        return int(response)


def run(B):
    oracle = Oracle()
    bits = [0] * B
    check_idx = -1
    check_map = {}
    last_reset = 0
    i = 0
    while i < B // 2:
        # print('i =', i, file=sys.stderr, flush=True)
        reset = False
        if oracle.is_it():
            # print('reset', i, file=sys.stderr, flush=True)
            reset = True
            if check_idx >= 0:
                b0 = oracle.query(check_idx + 1)
                b1 = oracle.query(check_idx + 2)
                f = check_map[str(b0) + str(b1)]
                # print('conv from', bits, file=sys.stderr, flush=True)
                bits = f(bits)
                # print('conv to  ', bits, file=sys.stderr, flush=True)
                pattern_bits = [bits[x] for x in (check_idx, check_idx+1, B-check_idx-2, B-check_idx-1)]
                pattern = ''.join(map(str, pattern_bits))
                check_map = MAPPING[pattern]
            else:
                last_reset = i
        bits[i] = oracle.query(i + 1)
        bits[B - i - 1] = oracle.query(B - i)
        if check_idx < 0 and i > 0 and not reset:
            pattern_bits = [bits[x] for x in (i - 1, i, B - i - 1, B - i)]
            pattern = ''.join(map(str, pattern_bits))
            if pattern in MAPPING:
                check_idx = i - 1
                check_map = MAPPING[pattern]
                # print('mapping found', check_idx, pattern, file=sys.stderr, flush=True)
        i += 1
        # print('bits', bits, file=sys.stderr, flush=True)
    # print('intermission, last_reset =', last_reset, 'check_idx =', check_idx, file=sys.stderr, flush=True)
    if check_idx >= 0:
        for i in range(last_reset):
            # print('i =', i, file=sys.stderr, flush=True)
            if oracle.is_it():
                # print('reset', i, file=sys.stderr, flush=True)
                b0 = oracle.query(check_idx + 1)
                b1 = oracle.query(check_idx + 2)
                f = check_map[str(b0) + str(b1)]
                # print('conv from', bits, file=sys.stderr, flush=True)
                bits = f(bits)
                # print('conv to  ', bits, file=sys.stderr, flush=True)
                pattern_bits = [bits[x] for x in (check_idx, check_idx+1, B-check_idx-2, B-check_idx-1)]
                pattern = ''.join(map(str, pattern_bits))
                check_map = MAPPING[pattern]
            bits[i] = oracle.query(i + 1)
            bits[B - i - 1] = oracle.query(B - i)
    else:
        for i in range(B//2 - 10, -1, -5):
            bb = [oracle.query(i + j + 1) for j in range(10)]
            f = detect(bits, i+5, bb[5:])
            bits[i+5:B-i-5] = f(bits[i+5:B-i-5])
            f = detect(bits, i, bb[:5])
            bits[i:i+5] = bb[:5]
            bits[B-i-5:B-i] = f(bits[i:B-i])[-5:]
    print(''.join(map(str, bits)), flush=True)
    response = sys.stdin.readline().strip()
    return response == 'Y'


T, B = [int(s) for s in sys.stdin.readline().split()]
for x in range(T):
    # print('\n\nSTART', x, file=sys.stderr, flush=True)
    if not run(B):
        break
