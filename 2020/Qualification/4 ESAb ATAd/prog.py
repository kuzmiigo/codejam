import sys


def complement(a):
    return [(x + 1) % 2 for x in a]


def reverse(a):
    return a[::-1]


def complement_reverse(a):
    return complement(reverse(a))


class Oracle:
    def __init__(self):
        self.num = 0

    def is_it(self):
        return (self.num + 1) % 10 == 1

    def query(self, i):
        self.num += 1
        print(i, flush=True)
        response = sys.stdin.readline().strip()
        # print('query =', self.num, 'get i =', i, 'response =', response,
        #       file=sys.stderr, flush=True)
        return int(response)


def run(B):
    oracle = Oracle()
    bits = [0] * B
    diff_idx = -1
    same_idx = -1
    for i in range(B // 2):
        if oracle.is_it():
            if diff_idx >= 0 and bits[diff_idx] != oracle.query(diff_idx + 1):
                bits = reverse(bits)
            if same_idx >= 0 and bits[same_idx] != oracle.query(same_idx + 1):
                bits = complement_reverse(bits)
            if (diff_idx < 0) != (same_idx < 0):
                oracle.query(1)  # alignment
        bits[i] = oracle.query(i + 1)
        bits[B - i - 1] = oracle.query(B - i)
        if diff_idx < 0 and bits[i] != bits[B - i - 1]:
            diff_idx = i
        if same_idx < 0 and bits[i] == bits[B - i - 1]:
            same_idx = i
    print(''.join(map(str, bits)), flush=True)
    response = sys.stdin.readline().strip()
    return response == 'Y'


T, B = [int(s) for s in sys.stdin.readline().split()]
for x in range(T):
    if not run(B):
        break
