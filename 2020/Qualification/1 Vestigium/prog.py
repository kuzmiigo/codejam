import sys


def process():
    N = int(sys.stdin.readline())  # matrix size
    k = 0  # trace of the matrix
    r = 0  # number of rows that contain repeated elements
    cols = [set() for _ in range(N)]
    for i in range(N):
        row = [int(s) for s in sys.stdin.readline().split()]
        if len(set(row)) != N:
            r += 1
        for j, m in enumerate(row):
            if i == j:
                k += m
            cols[j].add(m)
    # number of columns that contain repeated elements
    c = sum(1 for x in cols if len(x) != N)
    return k, r, c


T = int(sys.stdin.readline())
for x in range(T):
    print('Case #{}:'.format(x + 1), *process())
