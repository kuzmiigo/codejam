import sys


def insert_interval(free_time, person, result, interval):
    i, start, end = interval
    free_from = free_time.get(person, 0)
    if start < free_from:
        return False
    free_time[person] = end
    result[i] = person
    return True


def process():
    N = int(sys.stdin.readline())
    ranges = []
    for i in range(N):
        start, end = [int(s) for s in sys.stdin.readline().split()]
        ranges.append((i, start, end))
    result = [' '] * N
    free_time = {}
    for r in sorted(ranges, key=lambda t: (t[1], -t[2])):
        if insert_interval(free_time, 'C', result, r):
            continue
        if insert_interval(free_time, 'J', result, r):
            continue
        return 'IMPOSSIBLE'
    return ''.join(result)


T = int(sys.stdin.readline())
for x in range(T):
    print('Case #{}:'.format(x + 1), process())
