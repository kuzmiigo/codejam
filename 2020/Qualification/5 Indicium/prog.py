import sys


def try_square(square, N, K, num):
    if num == N * N:
        if sum([r[n] for n, r in enumerate(square)]) == K:
            return square
        else:
            return None
    i, j = divmod(num, N)
    taken = set([r[j] for r in square[:i]] + square[i][:j])
    if i == j:
        prev_trace = sum([r[n] for n, r in enumerate(square[:i])])
    else:
        prev_trace = 0
    for x in range(N, 0, -1):
        if x in taken:
            continue
        if i == j:
            if prev_trace + x > K:
                continue
            if K - prev_trace - x < N - i - 1:
                continue
        square[i][j] = x
        result = try_square(square, N, K, num + 1)
        if result:
            return result
    return None


def process():
    N, K = [int(s) for s in sys.stdin.readline().split()]
    square = [[0] * N for _ in range(N)]
    result = try_square(square, N, K, 0)
    if not result:
        return 'IMPOSSIBLE'
    return 'POSSIBLE\n' + '\n'.join([' '.join(map(str, r)) for r in square])


T = int(sys.stdin.readline())
for x in range(T):
    print('Case #{}:'.format(x + 1), process())
