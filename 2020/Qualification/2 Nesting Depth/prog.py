import sys


def adjust_level(s, cur_level, new_level):
    s += '(' * (new_level - cur_level)
    s += ')' * (cur_level - new_level)
    return s


def process():
    S = sys.stdin.readline().strip()
    result = ''
    depth = 0
    for c in S:
        d = int(c)
        result = adjust_level(result, depth, d)
        result += str(d)
        depth = d
    result = adjust_level(result, depth, 0)
    return result


T = int(sys.stdin.readline())
for x in range(T):
    print('Case #{}:'.format(x + 1), process())
